<h2><b>Odoo Developer Assessment - SIM Human Capital PT JIEP</b></h2>

# Deskripsi

Dalam Sistem Informasi Manajemen Human Capital yang dikembangkan untuk PT JIEP, terdapat
modul Rekrutmen yang fungsinya untuk merekrut kandidat hingga menjadi karyawan di PT JIEP.
Semua proses dilakukan di modul ini, mulai dari pengaturan lowongan pekerjaan di portal,
mendaftar lowongan, mengatur tahapan test, hingga membuat employee dari data kandidat.
Ada dua aktor yang terlibat dalam modul ini, yaitu Applicant dan Admin HCM.

Module ini mesti diselesaikan dalam tempo waktu yang terbatas, dan dalam hal ini saya berhasil menyelesaikan sekitar 70% dari final result yang diharapkan.

# Installasi

### Odoo Module
Letakkan module ini di dalam repository addons path yang ada di odoo.conf

> addons_path=/opt/odoo/odoo-server/addons,/opt/odoo/custom/addons

yang jika di lihat di local saya ada di repository /opt/odoo/custom/addons

pastikan ownership file sudah benar, jikalau masih ragu, maka eksekusi command berikut

> $ sudo chown -R odoo:odoo /opt/odoo/custom/addons/

sesuaikan user dan repository dengan apa yang ada di installasi local masing-masing

kemudian restart odoo.

![alt text](gama_hrms/static/src/img/image1.png "")

ketika anda membuka menu Apps di Odoo, anda seharusnya dapat menemukan module bernama "Custom recruitment"

jika module belum masuk ke dalam list, anda dapat menuju tombol "Update Apps List" di navbar.

### Konfigurasi Website

![alt text](gama_hrms/static/src/img/image2.png "")

setelah selesai melakukan installasi module, anda akan diminta melakukan installasi website, sesuaikan dengan preferensi anda.
dalam case ini, saya akan memilih "Skip and Start from Scratch" di pojok kanan bawah.

ini halaman pertama yang akan ditemukan.

![alt text](gama_hrms/static/src/img/image3.png "")

kita musti memunculkan tombol jobs, melalui "+New" di pojok kanan atas.
lalu pilih "Job Position"

![alt text](gama_hrms/static/src/img/image4.png "")

lalu lakukan edit pada halaman website dengan meng-klik "Edit" di pojok kanan atas.
lalu klik salah satu menu di navbar, dan pilih "Edit Menu"

![alt text](gama_hrms/static/src/img/image5.png "")

setelah muncul wizard dibawah ini, pilih "Add Menu Item"

![alt text](gama_hrms/static/src/img/image6.png "")

dan sesuaikan isian seperti dibawah ini

![alt text](gama_hrms/static/src/img/image7.png "")

 dan menu jobs akan muncul di website anda

 ![alt text](gama_hrms/static/src/img/image8.png "")

### Konfigurasi Stage

masuk ke halaman Recruitment - Configuration - Stages dan Sesuaikan Stage seperti dibawah ini

![alt text](gama_hrms/static/src/img/image9.png "")

# Halaman

### Applicant

![alt text](gama_hrms/static/src/img/image10.png "")

### Admin

![alt text](gama_hrms/static/src/img/image11.png "")