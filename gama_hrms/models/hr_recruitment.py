# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

# from random import randint

from odoo import api, fields, models, tools, SUPERUSER_ID
# from odoo.exceptions import AccessError, UserError
# from odoo.tools import Query
from odoo.tools.translate import _

from dateutil.relativedelta import relativedelta
from datetime import date

# from lxml import etree


class HrPengalaman(models.Model):
    _name = "hr.pengalaman"
    _description = "Pengalaman Kerja"

    #Data Pengalaman Kerja:
    nama_perusahaan = fields.Char("Nama Perusahaan", required=True)
    jabatan_terakhir = fields.Char("Jabatan Terakhir", required=True)
    tanggung_jawab = fields.Text('Deskripsi Jabatan/Tanggung Jawab', required=True)
    alasan_keluar = fields.Text('Alasan keluar', required=True)
    tanggal_gabung = fields.Date("Tanggal Gabung", required=True)
    tanggal_keluar = fields.Date("Tanggal Keluar", required=True)
    applicant_id = fields.Many2one('hr.applicant', "Recruitment")

class Applicant(models.Model):
    _inherit = "hr.applicant"

    @api.depends('tanggal_lahir')
    def _compute_usia(self):
        if self.tanggal_lahir:
            usia = relativedelta(date.today(), self.tanggal_lahir).years
            self.usia = usia
        else :
            self.usia = 0
    
    #Data Pribadi:
    nik = fields.Char("NIK", required=True)
    nik_file = fields.Binary(string="NIK File",attachment=True)
    tinggi = fields.Float("Tinggi", required=True)
    berat = fields.Float("Berat", required=True)
    user = fields.Char("User")
    jenis_kelamin = fields.Selection([
            ('laki', 'Laki-laki'),
            ('perempuan', 'Perempuan')], "Jenis Kelamin", required=True)
    agama = fields.Selection([
            ('islam', 'Islam'),
            ('kristen', 'Kristen'),
            ('katholik', 'Katholik'),
            ('lain', 'Lain-Lain')], "Agama")
    tempat_lahir = fields.Char("Tempat Lahir")
    tanggal_lahir = fields.Date("Tanggal Lahir", required=True)
    usia = fields.Float("Usia", compute=_compute_usia)
    alamat = fields.Text('Alamat', required=True)
    status_kawin = fields.Selection([
            ('menikah', 'Menikah'),
            ('belum', 'Belum Kawin'),
            ('cerai', 'Bercerai'),], "Status Perkawin")
    ptkp = fields.Selection([
            ('tk0', 'Wajib Pajak (TK0)'),
            ('tk1', 'Tanggungan 1 (TK1)'),
            ('tk2', 'Tanggungan 2 (TK2)'),
            ('tk3', 'Tanggungan 3 (TK3)')], "PTKP")
    #Pengalaman Kerja
    pengalaman_ids = fields.One2many('hr.pengalaman', 'applicant_id', 'Pengalaman Kerja')
    #Data Kualifikasi:
    pendidikan = fields.Selection([
            ('p1', 'SD / SMP / SMA'),
            ('p2', 'Diploma'),
            ('p3', 'Sarjana'),
            ('p4', 'S2/S3')], "Pendidikan Terakhir", required=True)
    ijazah = fields.Binary(string="Upload Ijazah",attachment=True, required=True)
    instansi_pendidikan = fields.Char("Nama Instansi Pendidikan Terakhir", required=True)
    jurusan = fields.Char("Jurusan", required=True)
    ipk = fields.Float("IPK", required=True)
    tahun_lulus = fields.Char("Tahun Lulus", required=True)
    cv = fields.Binary(string="CV",attachment=True, required=True)
    skck = fields.Binary(string="SKCK",attachment=True, required=True)

    #Data Lain-lain:
    nnpwp = fields.Char("NNPWP")
    nbpjs = fields.Char("NBPJS")
    expected_salary = fields.Char("Expected Salary", required=True)
    mengetahui_lowongan_dari = fields.Char("Mengetahui Lowongan dari", required=True)

    @api.depends('partner_id')
    def _compute_partner_phone_email(self):
        result = super(Applicant, self)._compute_partner_phone_email()
        for applicant in self:
            applicant.user = applicant.partner_id.name
        return result

#     @http.route(['/jobs/usia'], type='http', auth="user", csrf=False, methods= ['POST'])
#     def _calc_usia(self, csrf_token=False, date_from='', date_to=''):
#         _logger.info('Connection success')
#         _logger.info(csrf_token)
#         _logger.info(tanggal_lahir)
#         usia = 0
#         if not tanggal_lahir: 
#             usia = 0
#         else:
#             duration = relativedelta(date.today(), dt.datetime.strptime(tanggal_lahir, '%Y-%m-%d')).years
        
#         return usia 
