# -*- encoding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Custom Recruitment',
    'version': '1.1',
    'category': 'Human Resources/Recruitment',
    'sequence': 90,
    'summary': 'Custom recruitment module',
    'website': 'https://www.odoo.com/app/recruitment',
    'depends': [
        'hr_recruitment',
        'website_hr_recruitment',
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/hr_recruitment_views.xml',
        'data/config_data.xml',
        'views/website_hr_recruitment_templates.xml',
    ],
    'installable': True,
    'application': True,
    'license': 'LGPL-3',
}
